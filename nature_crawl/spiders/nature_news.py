from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from nature_crawl.items import NewsItem


class NatureNewsSpider(CrawlSpider):
    name = "natureNews"
    allowed_domains = ["nature.com"]
    start_urls = (
        'https://www.nature.com/nature/articles?type=news',
    )

    rules = [
        # 詳細ページをパースする。
        Rule(LinkExtractor(allow=r'/articles/d(-|[\d+])'),
             callback='parse_detail'),
    ]

    def parse_detail(self, response):
        """
        """

        # キーの値を指定してRestaurantオブジェクトを作成。
        item = NewsItem(
            title=response.css('article div.content header h1').xpath('normalize-space(text())').extract_first().strip()
            #address=response.css('[rel="address"]').xpath('string()').extract_first().strip(),
            ,body=response.css('article div.content div.article__body').xpath('string()').extract_first().strip()
        )

        yield item
